package net.lassau.ww.xml;

import java.util.ArrayList;
import java.util.List;

import net.lassau.ww.rpc.Game;
import net.lassau.ww.rpc.Headquarters;

import org.w3c.dom.Document;

public class XmlParser {

	public static Headquarters parseHeadquarters(Document doc) {
		XmlNode rootNode = new XmlNode(doc); 
		
		XmlNode gamesNode = rootNode.getChildNode("games");
		List<XmlNode> gameList = gamesNode.getChildNodes("game");
		List<Game> games = new ArrayList<Game>();
		for (XmlNode gameNode: gameList) {
			games.add(parseGame(gameNode));
		}
			
		return new Headquarters(games);
	}

	private static Game parseGame(XmlNode gameNode) {
		Game game = new Game();
		game.setInNeedOfAttention("true".equals(gameNode.getAttribute("inNeedOfAttention")));
		game.setId(gameNode.getChildNode("id").getValueAsInteger());
		game.setName(gameNode.getChildNode("name").getValue());
		game.setState(gameNode.getChildNode("state").getValue());
		game.setSince(gameNode.getChildNode("since").getValue());
		game.setRated(gameNode.getChildNode("rated").getValueAsBoolean());
		game.setLink(gameNode.getChildNode("link").getValue());
		game.setUrl(gameNode.getChildNode("url").getValue());
		game.setMap(gameNode.getChildNode("map").getValueAsInteger());
		game.setFactionState(gameNode.getChildNode("factionState").getValue());
		return game;
	}
}
