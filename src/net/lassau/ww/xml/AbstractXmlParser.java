package net.lassau.ww.xml;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;

public abstract class AbstractXmlParser {

	protected static Document parseDocument(String xml) {
		try {
			return DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new StringReader(xml)));
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}
}
