package net.lassau.ww.xml;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import net.lassau.ww.rpc.model.Hex;

public class CoordinateListParser extends AbstractXmlParser {

	public static Collection<Hex> parse(String xml) {
		XmlNode rootNode = new XmlNode(parseDocument(xml)).getFirstChild();
		if (!rootNode.getNodeName().equals("ok"))
			throw new IllegalStateException(xml);

		List<XmlNode> coordNodes = rootNode.getChildNodes("coordinate");
		Collection<Hex> hexes = new ArrayList<Hex>(coordNodes.size());
		for (XmlNode coordNode : coordNodes) {
			hexes.add(new Hex(coordNode.getAttributeAsInteger("x"), coordNode.getAttributeAsInteger("y")));
		}
		return hexes;
	}

}
