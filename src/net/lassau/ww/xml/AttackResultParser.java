package net.lassau.ww.xml;

import net.lassau.ww.rpc.model.AttackResult;

public class AttackResultParser extends AbstractXmlParser {

	public static AttackResult parse(String xml) {
//		<ok>
//		  <attack target="[4,2]" damageReceived="0" damageInflicted="6" remainingQuantity="4" />
//		  <finished />
//		</ok>		
		
		XmlNode okNode = new XmlNode(parseDocument(xml)).getChildNode("ok");
		if (okNode == null)
			throw new IllegalStateException(xml);

		XmlNode attackNode = okNode.getChildNode("attack");
		int damageReceived = attackNode.getAttributeAsInteger("damageReceived");
		int damageInflicted = attackNode.getAttributeAsInteger("damageInflicted");
		int remainingQuantity = attackNode.getAttributeAsInteger("remainingQuantity");
		boolean finished = okNode.getChildNode("finished") != null;

		return new AttackResult(damageReceived, damageInflicted, remainingQuantity, finished);
	}

}
