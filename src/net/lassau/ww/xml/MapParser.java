package net.lassau.ww.xml;

import java.io.Reader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilderFactory;

import net.lassau.ww.rpc.model.GameMap;
import net.lassau.ww.rpc.model.Hex;
import net.lassau.ww.rpc.model.Terrain;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;

public class MapParser {

	public static GameMap parseMap(Reader reader) {
		return parseMap(parseDocument(reader));	
	}
	private static Document parseDocument(Reader reader) {
		try {
			return DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(reader));
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}
	public static GameMap parseMap(Document doc) {
		XmlNode mapNode = new XmlNode(doc).getChildNode("map");
		GameMap gameMap = new GameMap();
		
		gameMap.setId(mapNode.getAttributeAsInteger("id"));
		gameMap.setName(mapNode.getChildNode("name").getValue());
		gameMap.setInitialCredits(mapNode.getChildNode("initialCredits").getValueAsInteger());
		gameMap.setPerBaseCredits(mapNode.getChildNode("perBaseCredits").getValueAsInteger());
		gameMap.setWidth(mapNode.getChildNode("width").getValueAsInteger());
		gameMap.setHeight(mapNode.getChildNode("height").getValueAsInteger());
		gameMap.setMaxPlayers(mapNode.getChildNode("maxPlayers").getValueAsInteger());
		gameMap.setUrl(mapNode.getChildNode("url").getValue());
		
		parseTerrains(mapNode.getChildNode("terrains").getChildNodes("terrain"), gameMap);
		
		return gameMap;	
	}

	private static void parseTerrains(List<XmlNode> terrainNodes, GameMap gameMap) {
		Map<Hex, Terrain> terrainMap = new HashMap<Hex, Terrain>();
		
		for (XmlNode terrainNode : terrainNodes) {
			// Extract the terrain type
			String terrainName = terrainNode.getAttribute("type");
			Hex hex = parseHexFromAttributes(terrainNode);
			terrainMap.put(hex, Terrain.valueOf(toUpper(terrainName)));			
		}
		
		gameMap.setTerrainMap(terrainMap);
	}

	static Hex parseHexFromAttributes(XmlNode xmlNode) {
		int x = xmlNode.getAttributeAsInteger("x");
		int y = xmlNode.getAttributeAsInteger("y");
		return new Hex(x, y);
	}

	private static String toUpper(String name) {
		return name.toUpperCase();
	}

}
