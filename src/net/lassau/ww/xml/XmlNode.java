package net.lassau.ww.xml;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XmlNode {
	private final Node node;

	public XmlNode(Node node) {
		this.node = node;
	}

	public List<XmlNode> getChildNodes(String childName) {
		List<XmlNode> children = new ArrayList<XmlNode>();
		NodeList nodeList = node.getChildNodes();
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node child = nodeList.item(i);
			if (child.getNodeName().equals(childName))
				children.add(new XmlNode(child));
		}
		return children;
	}

	public XmlNode getFirstChild() {
		return XmlNode.from(node.getFirstChild());
	}
	
	public XmlNode getChildNode(String childName) {
		List<XmlNode> children = getChildNodes(childName);
		switch (children.size()) {
		case 1:
			return children.get(0);
		case 0:
			return null;
		}
		throw new IllegalStateException("Too many child nodes found with name '" + childName + "'");
	}

	public String getAttribute(String name) {
		NamedNodeMap namedNodeMap = node.getAttributes();
		if (namedNodeMap == null)
			return null;
		Node att = namedNodeMap.getNamedItem(name);
		if (att == null)
			return null;
		else
			return att.getNodeValue();
	}
	
	public Integer getAttributeAsInteger(String name) {
		return parseInteger(getAttribute(name));
	}	
	
	private Integer parseInteger(String value) {
		if (value == null)
			return null;
		return Integer.parseInt(value);
	}

	public Boolean getAttributeAsBoolean(String name) {
		String value = getAttribute(name);
		if (value == null)
			return null;
		return value.equals("true");
	}	
	
	public boolean getAttributeAsBoolean(String name, boolean defaultValue) {
		String value = getAttribute(name);
		if (value == null)
			return defaultValue;
		return value.equals("true");
	}	
	
	public String getValue() {
		return node.getFirstChild().getNodeValue();
	}

	public Integer getValueAsInteger() {
		return parseInteger(getValue());
	}

	public boolean getValueAsBoolean() {
		return "true".equals(getValue());
	}

	public static XmlNode from(Node node) {
		return new XmlNode(node);
	}

	public String getNodeName() {
		return node.getNodeName();
	}

	@Override
	public String toString() {
		return "<" + getNodeName() + ">";
	}

}
