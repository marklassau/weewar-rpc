package net.lassau.ww.xml;

import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilderFactory;

import net.lassau.ww.rpc.GameState;
import net.lassau.ww.rpc.model.BaseCamp;
import net.lassau.ww.rpc.model.Faction;
import net.lassau.ww.rpc.model.Unit;
import net.lassau.ww.rpc.model.UnitType;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;

public class GameStateParser {

	public static GameState parseGameState(Reader reader) {
		return parseGameState(parseDocument(reader));	
	}
	private static Document parseDocument(Reader reader) {
		try {
			return DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(reader));
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}
	public static GameState parseGameState(Document doc) {
		XmlNode rootNode = new XmlNode(doc); 
		
		XmlNode gameNode = rootNode.getChildNode("game");
		GameState gameState = new GameState();
		gameState.setId(gameNode.getAttributeAsInteger("id"));
		gameState.setName(gameNode.getChildNode("name").getValue());
		gameState.setRound(gameNode.getChildNode("round").getValue());
		gameState.setState(gameNode.getChildNode("state").getValue());
		gameState.setPendingInvites(gameNode.getChildNode("pendingInvites").getValueAsBoolean());
		gameState.setPace(gameNode.getChildNode("pace").getValueAsInteger());
		gameState.setType(gameNode.getChildNode("type").getValue());
		gameState.setUrl(gameNode.getChildNode("url").getValue());
		gameState.setRated(gameNode.getChildNode("rated").getValueAsBoolean());
		parsePlayers(gameNode.getChildNode("players"), gameState);
		gameState.setDisabledUnitTypes(parseDisabledUnitTypes(gameNode.getChildNode("disabledUnitTypes")));
		gameState.setFactions(parseFactions(gameNode.getChildNode("factions")));
		gameState.setMap(gameNode.getChildNode("map").getValueAsInteger());
		gameState.setMapUrl(gameNode.getChildNode("mapUrl").getValue());
		gameState.setCreditsPerBase(gameNode.getChildNode("creditsPerBase").getValueAsInteger());
		gameState.setInitialCredits(gameNode.getChildNode("initialCredits").getValueAsInteger());
		gameState.setPlayingSince(gameNode.getChildNode("playingSince").getValue());
		return gameState;
	}

	private static List<String> parseDisabledUnitTypes(XmlNode disabledUnitTypesNode) {
		// <disabledUnitTypes>
	    //   <type>Heavy Trooper</type>
		//   <type>Speedboat</type>
		List<String> units = new ArrayList<String>();
		for (XmlNode typeNode : disabledUnitTypesNode.getChildNodes("type")) {
			units.add(typeNode.getValue());			
		}
		return units;
	}

	private static void parsePlayers(XmlNode playersNode, GameState game) {
//		<players>
//	    <player index="0" current="true">ai_MarsBot2</player>
//	    <player index="1">ai_ZappBrannigan</player>
//	  </players>
		List<String> players = new ArrayList<String>();
		for (XmlNode playerNode : playersNode.getChildNodes("player")) {
			String playerName = playerNode.getValue();
			players.add(playerName);			
			Boolean current = playerNode.getAttributeAsBoolean("current");
			if (current != null && current) {
				// check consistency
				if (game.getCurrentPlayer() == null)
					game.setCurrentPlayer(playerName);
				else
					throw new IllegalStateException("Multiple values found for current player");
			}
		}
		game.setPlayers(players);
		if (game.getCurrentPlayer() == null)
			throw new IllegalStateException("No values found for current player");
	}

	private static List<Faction> parseFactions(XmlNode factionsNode) {
		List<Faction> factions = new ArrayList<Faction>();
		for (XmlNode factionNode : factionsNode.getChildNodes("faction")) {
			factions.add(parseFaction(factionNode));			
		}
		return factions;
	}

	private static Faction parseFaction(XmlNode factionNode) {
		Faction faction = new Faction();
		faction.setCurrent(factionNode.getAttributeAsBoolean("current", false));
		faction.setCredits(factionNode.getAttributeAsInteger("credits"));
		faction.setType(factionNode.getAttribute("type"));
		faction.setEmailNotify(factionNode.getAttributeAsBoolean("emailNotify", false));
		faction.setPlayerId(factionNode.getAttributeAsInteger("playerId"));
		faction.setPlayerName(factionNode.getAttribute("playerName"));
		faction.setOfferPeace(factionNode.getAttributeAsBoolean("offerPeace"));
		faction.setState(factionNode.getAttribute("state"));
		// Units
		List<Unit> units = new ArrayList<Unit>();
		for (XmlNode unitNode : factionNode.getChildNodes("unit")) {
			units.add(parseUnit(unitNode));			
		}
		faction.setUnits(units);
		// BaseCamps
		List<BaseCamp> baseCamps = new ArrayList<BaseCamp>();
		for (XmlNode BaseCampNode : factionNode.getChildNodes("terrain")) {
			baseCamps.add(parseBaseCamp(BaseCampNode));			
		}
		faction.setBaseCamps(baseCamps);
//		List<Faction> factions = new ArrayList<Faction>();
//		for (XmlNode terrainNode : factionsNode.getChildNodes("terrain")) {
//			factions.add(parseFaction(factionNode));			
//		}
		
//	    <faction current="true" credits="400" type="user" emailNotify="true" playerId="108201" playerName="ai_MarsBot2" offerPeace="false" state="playing">
//	      <unit x="2" y="2" type="Trooper" quantity="10" finished="false" />
//	      <terrain x="1" y="2" type="Base" finished="false" />
//	    </faction>
//	    <faction playerId="35781" playerName="ai_ZappBrannigan" offerPeace="false" state="playing">
//	      <unit x="7" y="7" type="Trooper" quantity="10" finished="false" />
//	      <unit x="7" y="8" type="Trooper" quantity="10" finished="false" />
//	      <terrain x="7" y="6" type="Base" finished="false" />
//	    </faction>		
		
		return faction;
	}

	private static Unit parseUnit(XmlNode unitNode) {
		//	      <unit x="7" y="7" type="Trooper" quantity="10" finished="false" />
		Unit unit = new Unit();
		unit.setHex(MapParser.parseHexFromAttributes(unitNode));
		String type = unitNode.getAttribute("type");
		unit.setUnitType(UnitType.valueOf(toConstant(type)));
		unit.setQuantity(unitNode.getAttributeAsInteger("quantity"));
		unit.setFinished(unitNode.getAttributeAsBoolean("finished"));
		return unit;
	}
	static String toConstant(String type) {
		return type.toUpperCase().replace(' ', '_');
	}

	private static BaseCamp parseBaseCamp(XmlNode baseCampNode) {
		//	      <terrain x="7" y="6" type="Base" finished="false" />
		BaseCamp baseCamp = new BaseCamp();
		baseCamp.setHex(MapParser.parseHexFromAttributes(baseCampNode));
		baseCamp.setType(baseCampNode.getAttribute("type"));
		baseCamp.setFinished(baseCampNode.getAttributeAsBoolean("finished"));
		return baseCamp;
	}

}
