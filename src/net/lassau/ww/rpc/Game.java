package net.lassau.ww.rpc;

public class Game {
	public enum FactionState {PLAYING, CREATED}
	private boolean inNeedOfAttention;
	private int id;
	private String name;
	private String state;
	private String since;
	private boolean rated;
	private String link;
	private String url;
	private int map;
	private String factionState;
	
	public boolean isInNeedOfAttention() {
		return inNeedOfAttention;
	}
	public void setInNeedOfAttention(boolean inNeedOfAttention) {
		this.inNeedOfAttention = inNeedOfAttention;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getSince() {
		return since;
	}
	public void setSince(String since) {
		this.since = since;
	}
	public boolean isRated() {
		return rated;
	}
	public void setRated(boolean rated) {
		this.rated = rated;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public int getMap() {
		return map;
	}
	public void setMap(int map) {
		this.map = map;
	}
	public FactionState getFactionState() {
		return FactionState.valueOf(factionState.toUpperCase());
	}
	public String getFactionStateText() {
		return factionState;
	}
	public void setFactionState(String factionState) {
		this.factionState = factionState;
	}
	@Override
	public String toString() {
		return "Game [inNeedOfAttention=" + inNeedOfAttention + ", id=" + id
				+ ", name=" + name + ", state=" + state + ", since=" + since
				+ ", rated=" + rated + ", link=" + link + ", url=" + url
				+ ", map=" + map + ", factionState=" + factionState + "]";
	}
	
	
	
}
