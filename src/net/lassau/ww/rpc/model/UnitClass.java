package net.lassau.ww.rpc.model;

public enum UnitClass {
	SOFT, HARD, SPEEDBOAT, AMPHIBIC, BOAT, AIR, SUB
}
