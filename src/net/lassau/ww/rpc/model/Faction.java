package net.lassau.ww.rpc.model;

import java.util.Collection;
import java.util.Iterator;

import net.lassau.ww.rpc.util.CollectionUtil;
import net.lassau.ww.rpc.util.CollectionUtil.Filter;
import net.lassau.ww.rpc.util.Function;

public class Faction {
	private boolean current = false;
	private Integer credits;
	private String type;
	private boolean emailNotify;
	private int playerId;
	private String playerName;
	private boolean offerPeace;
	private String state;
	private Collection<Unit> units;
	private Collection<BaseCamp> baseCamps;
	
	public boolean isCurrent() {
		return current;
	}
	public void setCurrent(boolean current) {
		this.current = current;
	}
	public Integer getCredits() {
		return credits;
	}
	public void setCredits(Integer credits) {
		this.credits = credits;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public boolean isEmailNotify() {
		return emailNotify;
	}
	public void setEmailNotify(boolean emailNotify) {
		this.emailNotify = emailNotify;
	}
	public int getPlayerId() {
		return playerId;
	}
	public void setPlayerId(int playerId) {
		this.playerId = playerId;
	}
	public String getPlayerName() {
		return playerName;
	}
	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}
	public boolean isOfferPeace() {
		return offerPeace;
	}
	public void setOfferPeace(boolean offerPeace) {
		this.offerPeace = offerPeace;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public Collection<Unit> getUnits() {
		return units;
	}
	public Unit getUnit(Hex unitHex) {
		for (Unit unit : getUnits()) {
			if (unit.getHex().equals(unitHex))
				return unit;
		}
		return null;
	}	
	public Collection<Unit> getUnits(Collection<Hex> hexes) {
		return CollectionUtil.transform(hexes, new Function<Hex, Unit>() {
			@Override
			public Unit execute(Hex unitHex) {
				return getUnit(unitHex);
			}
		});
	}	
	public Collection<Unit> getUnitsOfType(final UnitType unitType) {
		return CollectionUtil.filter(getUnits(), new Filter<Unit>() {
			@Override
			public boolean accept(Unit unit) {
				return unit.getUnitType() == unitType;
			}
		});
	}
	public void setUnits(Collection<Unit> units) {
		this.units = units;
	}
	public Collection<BaseCamp> getBaseCamps() {
		return baseCamps;
	}
	public BaseCamp getBaseCamp(Hex base) {
		for (BaseCamp baseCamp : getBaseCamps()) {
			if (baseCamp.getHex().equals(base))
				return baseCamp;
		}
		return null;
	}
	public void setBaseCamps(Collection<BaseCamp> baseCamps) {
		this.baseCamps = baseCamps;
	}
	@Override
	public String toString() {
		final int maxLen = 10;
		return "Faction [current=" + current + ", credits=" + credits
				+ ", type=" + type + ", emailNotify=" + emailNotify
				+ ", playerId=" + playerId + ", playerName=" + playerName
				+ ", offerPeace=" + offerPeace + ", state=" + state
				+ ", units=" + (units != null ? toString(units, maxLen) : null)
				+ ", baseCamps="
				+ (baseCamps != null ? toString(baseCamps, maxLen) : null)
				+ "]";
	}
	private String toString(Collection<?> collection, int maxLen) {
		StringBuilder builder = new StringBuilder();
		builder.append("[");
		int i = 0;
		for (Iterator<?> iterator = collection.iterator(); iterator.hasNext()
				&& i < maxLen; i++) {
			if (i > 0)
				builder.append(", ");
			builder.append(iterator.next());
		}
		builder.append("]");
		return builder.toString();
	}
}
