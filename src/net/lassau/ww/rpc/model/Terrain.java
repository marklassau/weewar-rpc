package net.lassau.ww.rpc.model;

public enum Terrain {
	PLAINS, WOODS, MOUNTAINS, DESERT, SWAMP, WATER, BASE 
}
