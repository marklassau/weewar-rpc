package net.lassau.ww.rpc.model;

public class AttackResult {

	private final int damageReceived;
	private final int damageInflicted;
	private final int remainingQuantity;
	private final boolean finished;

	public AttackResult(int damageReceived, int damageInflicted, int remainingQuantity, boolean finished) {
		this.damageReceived = damageReceived;
		this.damageInflicted = damageInflicted;
		this.remainingQuantity = remainingQuantity;
		this.finished = finished;
	}

	public int getDamageReceived() {
		return damageReceived;
	}

	public int getDamageInflicted() {
		return damageInflicted;
	}

	public int getRemainingQuantity() {
		return remainingQuantity;
	}

	public boolean isFinished() {
		return finished;
	}	
}
