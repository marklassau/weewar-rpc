package net.lassau.ww.rpc.model;

import java.util.ArrayList;
import java.util.Collection;

public class Hex {
	private final int x, y;
	
	public Hex(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public Collection<Hex> getNeighbours() {
		Collection<Hex> neighbours = new ArrayList<Hex>(6);
		neighbours.add(new Hex(x + 1, y));
		neighbours.add(new Hex(x - 1, y));
		neighbours.add(new Hex(x + leftDelta(), y-1));
		neighbours.add(new Hex(x + rightDelta(), y-1));
		neighbours.add(new Hex(x + leftDelta(), y+1));
		neighbours.add(new Hex(x + rightDelta(), y+1));
		return neighbours;
	}

	private int leftDelta() {
		return rightDelta() - 1;
	}

	private int rightDelta() {
		return y % 2;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Hex other = (Hex) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "(" + x + ", " + y + ")";
	}

	public int distanceTo(Hex other) {
		if (this.x == other.x)
			return Math.abs(this.y - other.y);
		if (this.y == other.y)
			return Math.abs(this.x - other.x);
		// find next hop
		int nextX, nextY;
		if (x > other.x) {
			// go left
			nextX = x + leftDelta();
		} else {
			// go right
			nextX = x + rightDelta();
		}
		if (y > other.y) {
			// go up
			nextY = y - 1;
		} else {
			// go down
			nextY = y + 1;
		}
		return 1 + new Hex(nextX, nextY).distanceTo(other);
	}
	
	
	

}
