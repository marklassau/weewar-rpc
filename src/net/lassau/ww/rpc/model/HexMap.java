package net.lassau.ww.rpc.model;

import java.util.Map;


public interface HexMap<V> extends Map<Hex, V> {

	V getFor(Unit unit);

}
