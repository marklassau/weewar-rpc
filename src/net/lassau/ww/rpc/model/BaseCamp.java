package net.lassau.ww.rpc.model;

/** 
 * Represents a "Base" or Harbour that the user can create other units from
 */
public class BaseCamp {
	private Hex hex;
	private String type;
	private boolean finished;
	
	public Hex getHex() {
		return hex;
	}
	public void setHex(Hex hex) {
		this.hex = hex;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public boolean isFinished() {
		return finished;
	}
	public void setFinished(boolean finished) {
		this.finished = finished;
	}
	
	@Override
	public String toString() {
		return type + hex;
	}	
	
	
}
