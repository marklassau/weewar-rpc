package net.lassau.ww.rpc.model;

public class Unit {
	private Hex hex;
	private UnitType unitType;
	private int quantity = 10;
	private boolean finished;
	
	public Unit() {
	}
	
	public Unit(UnitType unitType, Hex hex) {
		this.unitType = unitType;
		this.hex = hex;
	}
	
	public Hex getHex() {
		return hex;
	}
	public void setHex(Hex hex) {
		this.hex = hex;
	}
	public UnitType getUnitType() {
		return unitType;
	}
	public void setUnitType(UnitType unitType) {
		this.unitType = unitType;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public boolean isFinished() {
		return finished;
	}
	public void setFinished(boolean finished) {
		this.finished = finished;
	}

	@Override
	public String toString() {
		return unitType.getName() + " " + hex;
	}				
}
