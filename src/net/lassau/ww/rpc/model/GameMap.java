package net.lassau.ww.rpc.model;

import java.util.Map;

public class GameMap {
	private int id;
	private String name;
	private int initialCredits;
	private int perBaseCredits;
	private int width;
	private int height;
	private int maxPlayers;
	private String url;
	private Map<Hex, Terrain> terrainMap;	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getInitialCredits() {
		return initialCredits;
	}
	public void setInitialCredits(int initialCredits) {
		this.initialCredits = initialCredits;
	}
	public int getPerBaseCredits() {
		return perBaseCredits;
	}
	public void setPerBaseCredits(int perBaseCredits) {
		this.perBaseCredits = perBaseCredits;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public int getMaxPlayers() {
		return maxPlayers;
	}
	public void setMaxPlayers(int maxPlayers) {
		this.maxPlayers = maxPlayers;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Map<Hex, Terrain> getTerrainMap() {
		return terrainMap;
	}
	public void setTerrainMap(Map<Hex, Terrain> terrainMap) {
		this.terrainMap = terrainMap;
	}

	
}
