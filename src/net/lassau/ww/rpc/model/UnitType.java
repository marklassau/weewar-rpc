package net.lassau.ww.rpc.model;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public enum UnitType {
	// http://weewar.wikispaces.com/Units
	TROOPER("Trooper", UnitClass.SOFT, 75, 9, true, 1, 1),
	HEAVY_TROOPER("Heavy Trooper", UnitClass.SOFT, 150, 6, true, 1, 1),
	RAIDER("Raider", UnitClass.HARD, 200, 12, true, 1, 1),
	TANK("Tank", UnitClass.HARD, 300, 9, true, 1, 1),
	LIGHT_ARTILLERY("Light Artillery", UnitClass.HARD, 200, 9, false, 2, 3);
	
	private final String name;
	private final UnitClass unitClass;
	private final int cost;
	private final int movementPoints;
	private final boolean canAttackAfterMoving;
	private final int minAttackRange;
	private final int maxAttackRange;
	
	UnitType(String name, UnitClass unitClass, int cost, int movementPoints, boolean canAttackAfterMoving, int minAttackRange, int maxAttackRange) {
		this.name = name;
		this.unitClass = unitClass;
		this.cost = cost;
		this.movementPoints = movementPoints;
		this.canAttackAfterMoving = canAttackAfterMoving;
		this.minAttackRange = minAttackRange;
		this.maxAttackRange = maxAttackRange;
	}
	
	public String getName() {
		return name;
	}
	
	public UnitClass getUnitClass() {
		return unitClass;
	}

	public int getCost() {
		return cost;
	}
	
	public int getMovementPoints() {
		return movementPoints;
	}
	
	public Integer getMovementCost(Terrain terrain) {
		return getMovementCost(this, terrain);
	}
	
	public boolean canAttackAfterMoving() {
		return canAttackAfterMoving;
	}

	public int getMinAttackRange() {
		return minAttackRange;
	}

	public int getMaxAttackRange() {
		return maxAttackRange;
	}

	public boolean hasZoneOfControlOn(UnitType unitType) {
		return hasZoneOfControlOn().contains(unitType.getUnitClass());
	}
	
	private static final Collection<UnitClass> TROOPER_ZOC = Arrays.asList(UnitClass.SOFT, UnitClass.HARD, UnitClass.SPEEDBOAT, UnitClass.AMPHIBIC, UnitClass.BOAT);
	private static final Collection<UnitClass> RAIDER_ZOC = Arrays.asList(UnitClass.SOFT, UnitClass.HARD, UnitClass.AIR, UnitClass.SPEEDBOAT, UnitClass.AMPHIBIC, UnitClass.BOAT);
	public Collection<UnitClass> hasZoneOfControlOn() {
		switch (this) {
		case TROOPER:
		case TANK:
			// This unit has ZoC on: Soft, Hard, Speedboat, Amphibic, Boat
			return TROOPER_ZOC; //
		case HEAVY_TROOPER:
		case RAIDER:
			// This unit has ZoC on: Soft, Hard, Air, Speedboat, Amphibic, Boat
			return RAIDER_ZOC;
		case LIGHT_ARTILLERY:
			// Nothing
			return Collections.emptyList();
		}
		throw new UnsupportedOperationException("Boom!");
	}
	
	private static Integer getMovementCost(UnitType unitType, Terrain terrain) {
		switch (unitType) {
		case TROOPER:
			switch (terrain) {
			case PLAINS:
				return 3;
			case WOODS:
				return 4;
			case MOUNTAINS:
				return 6;
			case DESERT:
				return 5;
			case SWAMP:
				return 6;
			case WATER:
				return null;
			case BASE:
				return 3;
			}
		case HEAVY_TROOPER:
			switch (terrain) {
			case PLAINS:
				return 3;
			case WOODS:
				return 4;
			case MOUNTAINS:
				return 6;
			case DESERT:
				return 5;
			case SWAMP:
				return 6;
			case WATER:
				return null;
			case BASE:
				return 3;
			}
		case RAIDER:
			switch (terrain) {
			case PLAINS:
				return 3;
			case WOODS:
				return 6;
			case MOUNTAINS:
				return null;
			case DESERT:
				return 4;
			case SWAMP:
				return 6;
			case WATER:
				return null;
			case BASE:
				return 2;
			}
		case TANK:
			switch (terrain) {
			case PLAINS:
				return 3;
			case WOODS:
				return 6;
			case MOUNTAINS:
				return null;
			case DESERT:
				return 4;
			case SWAMP:
				return 6;
			case WATER:
				return null;
			case BASE:
				return 2;
			}
		case LIGHT_ARTILLERY:
			switch (terrain) {
			case PLAINS:
				return 3;
			case WOODS:
				return 6;
			case MOUNTAINS:
				return null;
			case DESERT:
				return 4;
			case SWAMP:
				return 6;
			case WATER:
				return null;
			case BASE:
				return 2;
			}
		}
		throw new UnsupportedOperationException("Unknown movement cost for  " + unitType + " to move to " + terrain);
	}

	public int getDefenseStrength() {
		switch (this) {
		case TROOPER:
			return 6;
		case HEAVY_TROOPER:
			return 6;
		case TANK:
			return 10;
		case RAIDER:
			return 8;
		case LIGHT_ARTILLERY:
			return 3;
		}
		throw new UnsupportedOperationException("Unknown DefenseStrength for  " + this);
	}
	
	public int getAttackStrengthAgainst(UnitType defenderUnitType) {
		switch (this) {
		case TROOPER:
			switch (defenderUnitType.getUnitClass()) {
			case HARD:
				return 3;
			case SOFT:
				return 6;
			case SUB:
				return 0;
			case BOAT:
				return 3;
			case AMPHIBIC:
				return 3;
			case AIR:
				return 0;
			case SPEEDBOAT:
				return 3;
			}
		case HEAVY_TROOPER:
			switch (defenderUnitType.getUnitClass()) {
			case HARD:
				return 8;
			case SOFT:
				return 6;
			case SUB:
				return 0;
			case BOAT:
				return 8;
			case AMPHIBIC:
				return 8;
			case AIR:
				return 6;
			case SPEEDBOAT:
				return 8;
			}
		case RAIDER:
			switch (defenderUnitType.getUnitClass()) {
			case HARD:
				return 4;
			case SOFT:
				return 10;
			case SUB:
				return 0;
			case BOAT:
				return 4;
			case AMPHIBIC:
				return 8;
			case AIR:
				return 4;
			case SPEEDBOAT:
				return 4;
			}
		case TANK:
			switch (defenderUnitType.getUnitClass()) {
			case HARD:
				return 7;
			case SOFT:
				return 10;
			case SUB:
				return 0;
			case BOAT:
				return 7;
			case AMPHIBIC:
				return 7;
			case AIR:
				return 0;
			case SPEEDBOAT:
				return 7;
			}
		case LIGHT_ARTILLERY:
			switch (defenderUnitType.getUnitClass()) {
			case HARD:
				return 4;
			case SOFT:
				return 10;
			case SUB:
				return 0;
			case BOAT:
				return 4;
			case AMPHIBIC:
				return 4;
			case AIR:
				return 0;
			case SPEEDBOAT:
				return 4;
			}
		}
		throw new UnsupportedOperationException("Unknown getAttackStrengthAgainst for  " + this + " Terrain: " + defenderUnitType);
	}

	public Integer getTerrainAttackEffect(Terrain terrain) {
			switch (terrain) {
			case BASE:
				switch (unitClass) {
				case HARD:
					return 0;
				case SOFT:
					return 2;
				case SUB:
					return 0;
				case BOAT:
					return 0;
				case AMPHIBIC:
					return 0;
				case AIR:
					return 0;
				case SPEEDBOAT:
					return 0;
				}
			case DESERT:
				switch (unitClass) {
				case HARD:
					return 0;
				case SOFT:
					return -1;
				case SUB:
					return 0;
				case BOAT:
					return 0;
				case AMPHIBIC:
					return 0;
				case AIR:
					return 0;
				case SPEEDBOAT:
					return 0;
				}
			case MOUNTAINS:
				switch (unitClass) {
				case HARD:
					return -10;
				case SOFT:
					return 2;
				case SUB:
					return 0;
				case BOAT:
					return 0;
				case AMPHIBIC:
					return 0;
				case AIR:
					return 0;
				case SPEEDBOAT:
					return 0;
				}
			case PLAINS:
				switch (unitClass) {
				case HARD:
					return 0;
				case SOFT:
					return 0;
				case SUB:
					return 0;
				case BOAT:
					return 0;
				case AMPHIBIC:
					return 0;
				case AIR:
					return 0;
				case SPEEDBOAT:
					return 0;
				}
			case SWAMP:
				switch (unitClass) {
				case HARD:
					return -1;
				case SOFT:
					return -1;
				case SUB:
					return 0;
				case BOAT:
					return 0;
				case AMPHIBIC:
					return 0;
				case AIR:
					return 0;
				case SPEEDBOAT:
					return 0;
				}
			case WATER:
				switch (unitClass) {
				case HARD:
					return -10;
				case SOFT:
					return -10;
				case SUB:
					return 0;
				case BOAT:
					return 0;
				case AMPHIBIC:
					return 0;
				case AIR:
					return 0;
				case SPEEDBOAT:
					return 0;
				}
			case WOODS:
				switch (unitClass) {
				case HARD:
					return 0;
				case SOFT:
					return 2;
				case SUB:
					return 0;
				case BOAT:
					return 0;
				case AMPHIBIC:
					return 0;
				case AIR:
					return 0;
				case SPEEDBOAT:
					return 0;
				}
			}
		throw new UnsupportedOperationException("Unknown getTerrainAttackEffect for  " + this + " Terrain: " + terrain);
	}	
	

	public Integer getTerrainDefenseEffect(Terrain terrain) {
		switch (this) {
		case TROOPER:
		case HEAVY_TROOPER:
			switch (terrain) {
			case PLAINS:
				return 0;
			case WOODS:
				return 3;
			case MOUNTAINS:
				return 4;
			case DESERT:
				return -1;
			case SWAMP:
				return -2;
			case WATER:
				return null;
			case BASE:
				return 2;
			}
		case RAIDER:
		case TANK:
		case LIGHT_ARTILLERY:
			switch (terrain) {
			case PLAINS:
				return 0;
			case WOODS:
				return -3;
			case MOUNTAINS:
				return null;
			case DESERT:
				return 0;
			case SWAMP:
				return -2;
			case WATER:
				return null;
			case BASE:
				return -1;
			}		
		}
		throw new UnsupportedOperationException("Unknown TerrainDefenseEffect for  " + this + " Terrain: " + terrain);
	}	

}
