package net.lassau.ww.rpc;

import java.io.IOException;
import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilderFactory;

import net.lassau.ww.rpc.model.GameMap;
import net.lassau.ww.rpc.model.Hex;
import net.lassau.ww.rpc.model.UnitType;
import net.lassau.ww.xml.GameStateParser;
import net.lassau.ww.xml.MapParser;
import net.lassau.ww.xml.XmlParser;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;

public class Connection {
	Logger log = Logger.getLogger(Connection.class.getName());

	private final String authorizationString;

	public Connection(String authorizationString) {
		this.authorizationString = authorizationString;
	}

	public void close() {
	}
	
	public Headquarters getHeadquarters() {
		Document doc = requestDom("http://weewar.com/api1/headquarters");
		return XmlParser.parseHeadquarters(doc);
	}

	public GameState getGameState(Game game) throws IOException {
		String responseBody = makeRequest("http://weewar.com/api1/gamestate/" + game.getId());
		Document doc = parseDocument(responseBody);
		GameState gameState = GameStateParser.parseGameState(doc);
		gameState.setXml(responseBody);
		
		return gameState;
	}

	public GameMap getMap(int mapId) throws IOException {
		Document doc = requestDom("http://weewar.com/api1/map/" + mapId);
		return MapParser.parseMap(doc);
	}
	
	public void acceptInvitation(Game game) throws IOException {
		gameOperation("acceptInvitation", game);
	}
	
	public void declineInvitation(Game game) throws IOException {
		gameOperation("declineInvitation", game);
	}
	
	public void sendReminder(Game game) throws IOException {
		gameOperation("sendReminder", game);
	}
	
	public void surrender(Game game) throws IOException {
		gameOperation("surrender", game);
	}
	
	public void abandon(Game game) throws IOException {
		gameOperation("abandon", game);
	}
	
	public void removeGame(Game game) throws IOException {
		gameOperation("removeGame", game);
	}
	
	private void gameOperation(String operation, Game game) throws IOException {
		// eg <weewar game='7'><acceptInvitation/></weewar>
		String response = makePostRequest("http://weewar.com/api1/eliza", 
				"<weewar game='" + game.getId() + "'><" + operation + "/></weewar>");
		if (!response.contains("<ok />")) {
			throw new RuntimeException("Failed: " + response);
		}
	}

	public boolean move(int gameId, Hex unitHex, Hex moveHex) throws IOException {
		return moveAnd(gameId, unitHex, moveHex, ""); 
	}

	public boolean moveCapture(int gameId, Hex unitHex, Hex moveHex) throws IOException {
		return moveAnd(gameId, unitHex, moveHex, "<capture />"); 
	}
	
	private boolean moveAnd(int gameId, Hex unitHex, Hex moveHex, String secondOperation) throws IOException {
//		<weewar game='7'>
//		<unit x='3' y='5'>
//		<move x='3' y='12' />
//		</unit>	
//		</weewar>
		
		String response = makePostRequest("http://weewar.com/api1/eliza", 
				"<weewar game='" + gameId + "'>" + 
				"<unit x='" + unitHex.getX() + "' y='" + unitHex.getY() + "'>" + 
				"<move x='" + moveHex.getX() + "' y='" + moveHex.getY() + "' />" + 
				secondOperation +
				"</unit>" + 
				"</weewar>");
		log("Move " + unitHex + " result: " + response);
		if (response.contains("<finished />")) {
			return true;
		}
		throw new UnsupportedOperationException("Unknown response: " + response);
	}

	public void buildUnit(int gameId, UnitType unitType, Hex base) throws IOException {
		// <weewar game='7' ><build x='3' y='5' type='tank' /></weewar>
		String response = makeElizaRequest(gameId,
				"<build " + hexAsAttributes(base) + " type='" + unitType.name() + "' />"
				);
		log.info("buildUnit " + base + " result: " + response);
		if (response.contains("<ok />")) 
			return;
		if (response.contains("<error>"))
			throw new IllegalStateException(response);
		else
			throw new UnsupportedOperationException("Unknown response: " + response);
	}	

	private String makeElizaRequest(int gameId, String payload) throws IOException {
		return makePostRequest("http://weewar.com/api1/eliza", 
				"<weewar game='" + gameId + "'>" + 
				payload + 
				"</weewar>");
	}

	/** 
	 * Builds attributes for a Hex eg: "x='3' y='5'".
	 */
	private String hexAsAttributes(Hex hex) {
		return "x='" + hex.getX() + "' y='" + hex.getY() + "'";
	}

	private Document requestDom(String requestUrl) {
		String responseBody = makeRequest(requestUrl);
		if (log.isLoggable(Level.FINE)) {
			log.fine("request: " + requestUrl);
	        System.out.println("----------------------------------------");
	        System.out.println(responseBody);
	        System.out.println("----------------------------------------");
		}
		return parseDocument(responseBody);
	}

	private Document parseDocument(String xml) {
		try {
			return DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new StringReader(xml)));
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	private String makePostRequest(String requestUrl, String requestBody) throws IOException {  
//		curl -u AI_Bert:Gji3duvVQJghaJVvAwYGdPXRf -H 'Content-Type: application/xml' -d '<weewar game='3'>
//		<unit x='3' y='7' ><repair/></unit></weewar>' http://weewar.com/api1/eliza		
		
		HttpRequest req = HttpRequest.buildPostRequest(requestUrl, requestBody);
		req.setBasicAuthEncoded(authorizationString);
		req.setContentType("application/xml");
		HttpResponse resp = req.sendRequest();
		if (resp.getResponseCode() == 200) {
			return resp.getResponseAsString();
		} else {
			throw new RuntimeException("Request failed: " + resp.getResponseCode() + " " + 
							resp.getResponseMessage() + " " + resp.getResponseAsString());
		}		
	}

	private String makeRequest(String requestUrl) {  
		HttpRequest req = HttpRequest.buildGetRequest(requestUrl);
		req.setBasicAuthEncoded(authorizationString);
		req.setContentType("application/xml");
		HttpResponse resp = req.sendRequest();
		if (resp.getResponseCode() == 200) {
			return resp.getResponseAsString();
		} else {
			throw new RuntimeException("Request failed: " + resp.getResponseCode() + " " + 
							resp.getResponseMessage() + " " + resp.getResponseAsString());
		}		
	}
	
	private void log(String message) {
		System.out.println(message);
	}
	
	public class RuntimeIOException extends RuntimeException {
		public RuntimeIOException(IOException ex) {
			super(ex);
			
		}
	}

	/**
	 * Creates a connection object for making requests about a particular given gameId.
	 * 
	 * @return the newly created GameConnection
	 */
	public GameConnection getGameConnection(Game game) {
		game.getMap();
		return getGameConnection(game.getId());
	}

	/**
	 * Creates a connection object for making requests about a particular given gameId.
	 * 
	 * @return the newly created GameConnection
	 */
	public GameConnection getGameConnection(int gameId) {
		return new GameConnection(authorizationString, gameId);
	}
}
