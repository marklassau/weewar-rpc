package net.lassau.ww.rpc;

import java.util.Collection;

public class Headquarters {
	private final Collection<Game> games;
	
	public Headquarters(Collection<Game> games) {
		this.games = games;
	}

	public Collection<Game> getGames() {
		return games;
	}

	@Override
	public String toString() {
		return "Headquarters [games=" + games + "]";
	}
	
	
}
