package net.lassau.ww.rpc.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class CollectionUtil {

	public interface Filter<T> {
		boolean accept(T t);
	}
	
	public static class MultiFilter<T> implements Filter<T> {
		private final Filter<T>[] filters;

		public MultiFilter(Filter<T>... filters) {
			this.filters = filters;
		}

		@Override
		public boolean accept(T t) {
			for (Filter<T> filter : filters) {
				if (!filter.accept(t)) {
					return false;
				}
			}
			return true;
		}
		
	}

	public static <T> List<T> filter(Collection<T> objects, Filter<T> filter) {
		List<T> filteredList = new LinkedList<T>();
		for (T t : objects) {
			if (filter.accept(t))
				filteredList.add(t);
		}
		return filteredList;
	}

	public static <T> List<T> filter(Collection<T> objects, Filter<T> filter1, Filter<T> filter2) {
		List<T> filteredList = new LinkedList<T>();
		for (T t : objects) {
			if (filter1.accept(t) && filter2.accept(t))
				filteredList.add(t);
		}
		return filteredList;
	}

	public static <I, O> List<O> transform(Collection<I> input, Function<I, O> function) {
		List<O> output = new ArrayList<O>(input.size());
		for (I i : input) {
			output.add(function.execute(i));
		}
		return output ;
	}

	public static <K, V> Map<K, V> map(Collection<K> keys, Function<K, V> function) {
		Map<K, V> map = new HashMap<K, V>(keys.size());
		for (K k : keys) {
			map.put(k, function.execute(k));
		}
		return map;
	}

}
