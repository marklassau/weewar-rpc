package net.lassau.ww.rpc.util;

public interface Function<I, O> {
	O execute(I i);
}
