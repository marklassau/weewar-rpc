package net.lassau.ww.rpc;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class HttpRequest {
	private final Method requestMethod;
	private final String requestUrl;
	private final String requestBody;
	private String authorizationString;
	private String contentType;

	public enum Method {GET, POST}

	public HttpRequest(Method requestMethod, String requestUrl) {
		this.requestMethod = requestMethod;
		this.requestUrl = requestUrl;
		this.requestBody = null;
	}

	public HttpRequest(Method requestMethod, String requestUrl, String requestBody) {
		this.requestMethod = requestMethod;
		this.requestUrl = requestUrl;
		this.requestBody = requestBody;
	}

	public void setBasicAuthEncoded(String authorizationString) {
		this.authorizationString = authorizationString;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public HttpResponse sendRequest() {
		try {
	        // Build the request
			URL url = new URL(requestUrl);
	        HttpURLConnection connection = (HttpURLConnection) url.openConnection();           
	        connection.setRequestMethod(requestMethod.name()); 
	        if (authorizationString != null)
	        	connection.setRequestProperty("Authorization", authorizationString);
	        if (contentType != null)
	        	connection.setRequestProperty("Content-Type", contentType);
	        if (requestBody != null) {
		        connection.setDoOutput(true);
		        OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
		        out.write(requestBody);
		        out.flush();	        
	        }
	        // Send the request
	        connection.connect();
	        return new HttpResponse(connection);
		} catch (MalformedURLException ex) {
			throw new RuntimeException(ex);
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}		
	}

	public static HttpRequest buildGetRequest(String requestUrl) {
		return new HttpRequest(Method.GET, requestUrl);
	}

	public static HttpRequest buildPostRequest(String requestUrl, String requestBody) {
		return new HttpRequest(Method.POST, requestUrl, requestBody);
	}
}
