package net.lassau.ww.rpc;

import java.io.IOException;
import java.io.StringReader;
import java.util.Collection;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilderFactory;

import net.lassau.ww.rpc.model.AttackResult;
import net.lassau.ww.rpc.model.Hex;
import net.lassau.ww.rpc.model.Unit;
import net.lassau.ww.rpc.model.UnitType;
import net.lassau.ww.xml.AttackResultParser;
import net.lassau.ww.xml.CoordinateListParser;
import net.lassau.ww.xml.GameStateParser;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;

public class GameConnection {
	private static Logger log = Logger.getLogger(GameConnection.class.getName());
	
	private final String authorizationString;
	private final int gameId;

	GameConnection(String authorizationString, int gameId) {
		this.authorizationString = authorizationString;
		this.gameId = gameId;
	}

	public GameState getGameState() throws IOException {
		String responseBody = makeGetRequest("http://weewar.com/api1/gamestate/" + gameId);
		Document doc = parseDocument(responseBody);
		GameState gameState = GameStateParser.parseGameState(doc);
		gameState.setXml(responseBody);
		
		return gameState;
	}

	public void finishTurn() {
		gameOperation("finishTurn");
	}
	
	public void surrender() {
		gameOperation("surrender");
	}
	
	private void gameOperation(String operation) {
		// eg <weewar game='7'><acceptInvitation/></weewar>
		String response = makePostRequest("http://weewar.com/api1/eliza", 
				"<weewar game='" + gameId + "'><" + operation + "/></weewar>");
		if (!response.contains("<ok />")) {
			throw new RuntimeException("Failed: " + response);
		}
	}

	public boolean move(Hex unitHex, Hex moveHex) {
		String response =  moveAnd(unitHex, moveHex, ""); 
		if (response.contains("<finished />")) {
			return true;
		}
		throw new UnsupportedOperationException("Unknown response: " + response);		
	}

	public AttackResult moveAttack(Hex unitHex, Hex moveHex, Hex attackHex) {
//		<weewar game='7'>
//		<unit x='3' y='5'>
//		<move x='3' y='12' />
//		<attack x='7' y='9' />
//		</unit>
//		</weewar>		
		String response = moveAnd(unitHex, moveHex, "<attack " + hexAsAttributes(attackHex) + " />"); 
		return AttackResultParser.parse(response);
	}

	public boolean moveCapture(Hex unitHex, Hex moveHex) {
		String response = moveAnd(unitHex, moveHex, "<capture />"); 
		if (response.contains("<finished />")) {
			return true;
		}
		throw new UnsupportedOperationException("Unknown response: " + response);		
	}
	
	private String moveAnd(Hex unitHex, Hex moveHex, String secondOperation) {
//		<weewar game='7'>
//		<unit x='3' y='5'>
//		<move x='3' y='12' />
//		</unit>
//		</weewar>

		final String moveOperation;
		if (moveHex == null)
			moveOperation = "";
		else
			moveOperation = "<move " + hexAsAttributes(moveHex) + " />";
		String response = makeElizaRequest( 
				"<unit " + hexAsAttributes(unitHex) + ">" + 			
				moveOperation + 
				secondOperation +
				"</unit>");
		log.fine("Move " + unitHex + " " + secondOperation + " result: " + response);
		return response;
	}

	public void buildUnit(UnitType unitType, Hex base) throws IOException {
		// <weewar game='7' ><build x='3' y='5' type='tank' /></weewar>
		String response = makeElizaRequest("<build " + hexAsAttributes(base) + " type='" + unitType.getName() + "' />");
		log.fine("buildUnit " + base + " result: " + response);
		if (response.contains("<ok />")) 
			return;
		if (response.contains("<error>"))
			throw new IllegalStateException(response);
		else
			throw new UnsupportedOperationException("Unknown response: " + response);
	}	

	public boolean repair(Hex unitHex) {
//		<weewar game='7'>
//		<unit x='3' y='5'>
//		<repair />
//		</unit>
//		</weewar>
		String response = makeElizaRequest("<unit " + hexAsAttributes(unitHex) + ">" + 
				"<repair />" + 
				"</unit>");
		log.fine("repair " + unitHex + " result: " + response);
		if (response.contains("<finished />")) {
			return true;
		}
		throw new UnsupportedOperationException("Unknown response: " + response);		
	}
	
	public Collection<Hex> getMovementOptions(Unit unit) {
		// <weewar game='7' ><movementOptions x="3" y="5" type="Heavy Trooper" /></weewar>
		String response = makeElizaRequest("<movementOptions " + hexAsAttributes(unit.getHex()) + " type='" + unit.getUnitType().getName() + "' />");
		log.fine("movementOptions " + unit + " result: " + response);
		return CoordinateListParser.parse(response);		
	}

	public Collection<Hex> getAttackOptions(Hex fromHex, UnitType unitType) {
		// <weewar game='7' ><attackOptions x="3" y="5" type="Heavy Trooper" moved="1" /></weewar>
		
		String response = makeElizaRequest( 
				"<attackOptions " + hexAsAttributes(fromHex) + " type='" + unitType.getName() + "' />"
				);
		log.fine("attackOptions " + fromHex + " result: " + response);
		return CoordinateListParser.parse(response);		
	}
	
	private String makeElizaRequest(String payload) {
		return makePostRequest("http://weewar.com/api1/eliza", 
				"<weewar game='" + gameId + "'>" + 
				payload + 
				"</weewar>");
	}

	/** 
	 * Builds attributes for a Hex eg: "x='3' y='5'".
	 */
	private String hexAsAttributes(Hex hex) {
		return "x='" + hex.getX() + "' y='" + hex.getY() + "'";
	}

	private Document parseDocument(String xml) {
		try {
			return DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new StringReader(xml)));
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	private String makePostRequest(String requestUrl, String requestBody) {  
//		curl -u AI_Bert:Gji3duvVQJghaJVvAwYGdPXRf -H 'Content-Type: application/xml' -d '<weewar game='3'>
//		<unit x='3' y='7' ><repair/></unit></weewar>' http://weewar.com/api1/eliza		
		
		HttpRequest req = HttpRequest.buildPostRequest(requestUrl, requestBody);
		req.setBasicAuthEncoded(authorizationString);
		req.setContentType("application/xml");
		HttpResponse resp = req.sendRequest();
		if (resp.getResponseCode() == 200) {
			return resp.getResponseAsString();
		} else {
			throw new RuntimeException("Request failed: " + resp.getResponseCode() + " " + 
							resp.getResponseMessage() + " " + resp.getResponseAsString());
		}		
	}

	private String makeGetRequest(String requestUrl) {  
		HttpRequest req = HttpRequest.buildGetRequest(requestUrl);
		req.setBasicAuthEncoded(authorizationString);
		req.setContentType("application/xml");
		HttpResponse resp = req.sendRequest();
		if (resp.getResponseCode() == 200) {
			return resp.getResponseAsString();
		} else {
			throw new RuntimeException("Request failed: " + resp.getResponseCode() + " " + 
							resp.getResponseMessage() + " " + resp.getResponseAsString());
		}		
	}
	
	public class RuntimeIOException extends RuntimeException {
		public RuntimeIOException(IOException ex) {
			super(ex);
			
		}
	}
}
