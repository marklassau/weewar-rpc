package net.lassau.ww.rpc;

import java.util.List;

import net.lassau.ww.rpc.model.Faction;

public class GameState {
	private int id;
	private String name;
	private String round;
	private String state;
	private boolean pendingInvites;
	private int pace;
	private String type;
	private String url;
	private boolean rated;
	private List<String> players;
	private String currentPlayer;
	private List<String> disabledUnitTypes;
	private List<Faction> factions;
	private int map;
	private String mapUrl;
	private int creditsPerBase;
	private int initialCredits;
	private String playingSince;
	private Faction myFaction;
	private Faction enemyFaction;
	private String xml;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRound() {
		return round;
	}
	public void setRound(String round) {
		this.round = round;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public boolean isPendingInvites() {
		return pendingInvites;
	}
	public void setPendingInvites(boolean pendingInvites) {
		this.pendingInvites = pendingInvites;
	}
	public int getPace() {
		return pace;
	}
	public void setPace(int pace) {
		this.pace = pace;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public boolean isRated() {
		return rated;
	}
	public void setRated(boolean rated) {
		this.rated = rated;
	}
	public List<String> getPlayers() {
		return players;
	}
	public void setPlayers(List<String> players) {
		this.players = players;
	}
	public List<String> getDisabledUnitTypes() {
		return disabledUnitTypes;
	}
	public void setDisabledUnitTypes(List<String> disabledUnitTypes) {
		this.disabledUnitTypes = disabledUnitTypes;
	}
	public int getMap() {
		return map;
	}
	public void setMap(int map) {
		this.map = map;
	}
	public String getMapUrl() {
		return mapUrl;
	}
	public void setMapUrl(String mapUrl) {
		this.mapUrl = mapUrl;
	}
	public int getCreditsPerBase() {
		return creditsPerBase;
	}
	public void setCreditsPerBase(int creditsPerBase) {
		this.creditsPerBase = creditsPerBase;
	}
	public int getInitialCredits() {
		return initialCredits;
	}
	public void setInitialCredits(int initialCredits) {
		this.initialCredits = initialCredits;
	}
	public String getPlayingSince() {
		return playingSince;
	}
	public void setPlayingSince(String playingSince) {
		this.playingSince = playingSince;
	}
	public List<Faction> getFactions() {
		return factions;
	}
	public void setFactions(List<Faction> factions) {
		if (factions.size() != 2)
			throw new IllegalArgumentException();
		this.factions = factions;
		for (Faction faction : factions) {
			if (faction.isCurrent())
				myFaction = faction;
			else
				enemyFaction = faction;
		}
	}
	public String getCurrentPlayer() {
		return currentPlayer;
	}
	public void setCurrentPlayer(String currentPlayer) {
		this.currentPlayer = currentPlayer;
	}	
	
	public String getXml() {
		return xml;
	}
	public void setXml(String xml) {
		this.xml = xml;
	}
	@Override
	public String toString() {
		return "GameDetails [id="
				+ id + ", name=" + name + ", round=" + round + ", state="
				+ state + ", pendingInvites=" + pendingInvites + ", pace="
				+ pace + ", type=" + type + ", url=" + url + ", rated=" + rated
				+ ", players=" + players + ", disabledUnitTypes="
				+ disabledUnitTypes + ", map=" + map + ", mapUrl=" + mapUrl
				+ ", creditsPerBase=" + creditsPerBase + ", initialCredits="
				+ initialCredits + ", playingSince=" + playingSince + "]";
	}
	public Faction getMyFaction() {
		return myFaction;
	}
	public Faction getEnemyFaction() {
		return enemyFaction;
	}	

}
