package net.lassau.ww.rpc;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;

public class HttpResponse {

	private final HttpURLConnection connection;

	public HttpResponse(HttpURLConnection connection) {
		this.connection = connection;
	}

	public int getResponseCode() {
    	try {
    		return connection.getResponseCode();
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}		
	}
	
	public String getResponseMessage() {
    	try {
    		return connection.getResponseMessage();
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}		
	}
	
	public String getResponseAsString() {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		InputStream inputStream = getInputStream();
		try {
			int b = inputStream.read();
			while (b != -1)
			{
				out.write(b);
				b = inputStream.read();
			}
			return out.toString();	
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		} finally {
			try {
				inputStream.close();
			} catch (IOException ignored) {
			}
		}
	}
	
    public InputStream getInputStream() {
    	try {
			return connection.getInputStream();
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
    }
}
